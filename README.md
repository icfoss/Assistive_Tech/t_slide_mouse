TSlide Mouse
============
TSlide mouse is a open source USB input device (HID) for Persons with
Disabilities (PwD). This repository contains the firmware and
configuration files for TSlide Mouse. Please refer to the [design document](https://gitlab.com/icfoss/Assistive_Tech/t_slide_mouse/tree/master/Design/Tslide_Mouse_Design_Documentation.pdf) 
for more details about the project.
![Tslide mouse](images/T_SLide_Mouse.jpg)

## The Hardware
The TSlide Mouse is built upon a Arduino ProMicro/ Micro(ATmega32U4) in the
current version. The components include thumbslide joystick, two touch
sensors and vibrator motor (optional) for haptic feeback.

An Arduino ProMicro/Micro is chosen mainly  due to its onboard controller.
ATmega32U4 can be easly programmed as a HID using the existing library that is
available in Arduino IDE (Mouse.h).

## Firmware
The firmware is based on Arduino Framework. Which make it easy to program
at same time powerfull enough for this particular purpose. The firmware
is powerfull enough to enable the device to be interpreted as USB HID,
which enable the device to used across platforms without the need of a
driver support.

## Build
The source files can be built and deployed from Arduino IDE.

## Quick Firmware Build Instructions
1.Make sure your [Arduino IDE](https://www.arduino.cc/en/Main/Software)
installation complete and the IDE is working [perfectly](https://www.arduino.cc/en/Guide/Troubleshooting)

2.Clone Repository
```
git clone https://gitlab.com/icfoss/Assistive_Tech/t_slide_mouse.git
```
3.Configure the Main program (Mouse.ino)

In order to configue the main program (Mouse.ino), first upload
Thumbslide_test.ino onto the Arduino ProMicro and run the serial monitor
under the tools tab. The serial monitor helps to display messages from
the device. Now follow the instruction given by the device through the
serial monitor.

After following all the instruction, the program will give 6 different
values which has to noted down. The vaariables are:

 1. top_val_x     

 2. bottom_val_x

 3. top_val_y     

 4. bottom_val_y

 5. x_center

 6. y_center

4.Configure the Main Program (Mouse.ino)

Now change the values of the corresponding variables that are present
in Mouse.ino file and upload the program on to the Arduino ProMicro.
Now unlock the Tslide Mouse and test the mouse.

## Quick Hardware Build Instructions
1. All the parts have to be 3D printed. (stl files uploaded in the Design Folder)
2. Use knife to chip of the support and excess material.
3. Connect the circuit as per shown in the diagram.
4. Brush the fevicol ( synthetic rubber adhesive ) on the
outer surface of the device and on the down side of the rexine.
5. Keep it to dry for 5 minutes.
6. Gently stick it to the surface , strip of the excess rexine.
7. Insert the Thumb slide through the groove provided for it.
8. Place the stopper in the position and use M3*5mm self thread screws tighten it.
9. Place the touch sensor and use
the glue stick to hold it in fixed position
( glue stick is enough because no pressure is exerted on the touch
sensor ).
10. Use glue stick to stick the top and lower part.


## License
Copyright (C) 2018  ICFOSS

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

A copy of the license is available at the root of this source tree.
For more details, see <http://www.gnu.org/licenses/>.
