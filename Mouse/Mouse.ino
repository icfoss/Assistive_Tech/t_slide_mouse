/*  Firmware for TSlide Mouse
    Copyright (C) 2018  ICFOSS

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*******************************************************************************
 * Program 2
 * *****************
 *The hardware for this project includes two touch tensors, one vibrator motor, 
  and a thumblide joystick.
 *Each of the touch sesnors are 
 *The vibration motor (optional) is used to provide haptic feedback to the user. It 
  is connected to pin asigned in the variable motor_pin 
 *The device is capable of moving the mouse cursor using the thumbslide joystick 
  connected to analog pins assigned in variables x_axis and y_axis.
 *Left and right click of TslideMouse is enabled using the two touch sensors 
  connected to the micro controller pins assigned in the variables left_bt and right_bt.

 *********************************************************************************
 *Note*
 *Before uploading this program, you have to adjust the top and bottom values of the 
  thumslide into the variable top_val ,*_center and bottom_val
 *Upload the program: Thumbslide_test.ino and note down the top, center and 
  bottom values of the thumbslide joystick module
 *********************************************************************************
*/
#include "Mouse.h"
int x, y;
int hardcodeSpeed = 8, buttondelay=120,motordelay=30; 
//hardcodeSpeed is the hardware emebeded thumbslide response of the mouse 
//button delay have to be tailored to the users convienence, it denotes the 
//          minimum time it takes for the user to press and release a button
//motor delay is the delay time for which the motor vibrates to provide haptic feeback

bool pressedl=0,pressedr=0,scrollr=0,scroll=0;

int left_bt=3 ,right_bt=2, motor_pin=5; 
// change variable to the pin to which you have connected
int y_axis = A3, x_axis = A2;  
// x and y axis of the thumbslide joystick is connected these analog pins
int top_val_x=790, bottom_val_x=50,top_val_y=790, bottom_val_y=95;
// the bottom and top values for the thumbslide joystick has to manually cross checked 
//using the program: Thumbslide_test.ino
int x_center=5,y_center=5;
long unsigned int lockTime=3000;
void motor()
{
  if(pressedl==1) //the global variable helps which button is pressed so as to apply the vibration pattern
  {
    digitalWrite(motor_pin,HIGH);
    delay(motordelay);
    digitalWrite(motor_pin,LOW);
  }

  if(pressedr==1) //the global variable helps which button is pressed so as to apply the vibration pattern
  {
    digitalWrite(motor_pin,HIGH);
    delay(motordelay);
    digitalWrite(motor_pin,LOW);
    delay(motordelay+20);
    digitalWrite(motor_pin,HIGH);
    delay(motordelay);
    digitalWrite(motor_pin,LOW);
  }
}

void setup()
{
  pinMode(right_bt, INPUT);
  pinMode(left_bt, INPUT);
  pinMode(motor_pin,OUTPUT);
  long unsigned int timed1,timed2;

  while((digitalRead(left_bt)!=HIGH)||(digitalRead(right_bt)!=HIGH));
  timed1 = millis();
  while (1)
  {
    if ((digitalRead(left_bt)==HIGH)&&(digitalRead(right_bt)==HIGH))
    {
      timed2=millis();
      if ((timed2-timed1)>=lockTime)
       break;
    }
    else
    {
      timed2=millis();
    }
    
  }
  
  Mouse.begin();
  pressedr=1; //for vibrating the motor twice
  motor();
  pressedr=0; // reintialise the varible
}

int xe,ye;
void loop()
{
  y = analogRead(y_axis);
  x = analogRead(x_axis);

  y = map(y,bottom_val_y, top_val_y, 0, 10);
  // the bottom and top values for the thumbslide joystick has to manually 
  //           cross checked using the program: Thumbslide_test.ino
  x = map(x,bottom_val_x, top_val_x, 0, 10);
  // the bottom and top values for the thumbslide joystick has to manually 
  //          cross checked using the program: Thumbslide_test.ino
  xe=ye=0;
  if (x<x_center-1)//"LEFT"
      xe=-1;
  if (x>x_center+1)//"Right"
      xe=1;
  if (y<y_center-1)//"DOWN"
      ye=1;
  if (y>x_center+1)//"UP"
      ye=-1;
  if (scroll==0)
  {  
    Mouse.move(xe, ye, 0);
    delay(hardcodeSpeed); 
  }
  else
  {
    Mouse.move(0, 0, ye);
    delay(2*buttondelay);
  }

  if((digitalRead(left_bt)==HIGH)&&(scroll==0)&&(digitalRead(right_bt)==HIGH))
    {
      scroll=1;
      pressedl=2;
      pressedr=2;
      motor();
      Mouse.release(MOUSE_RIGHT);
      Mouse.press(MOUSE_LEFT);
      delay(buttondelay);
      Mouse.release(MOUSE_LEFT);
      Mouse.release(MOUSE_RIGHT);
      
    }
   else if((digitalRead(left_bt)==LOW)&&(scroll==1)&&(digitalRead(right_bt)==LOW))
   {
    scroll=0;
    pressedl=0;
    pressedr=0;
    delay(3*buttondelay); 
   }
    
  if((digitalRead(left_bt)==HIGH)&&(pressedl==0)&&(pressedr==0))
    {
      pressedl=1;
      motor();
      Mouse.press(MOUSE_LEFT);
      delay(buttondelay);
    }

  if((digitalRead(left_bt)==LOW)&&(pressedl==1))
    {
      pressedl=0;
      Mouse.release(MOUSE_LEFT);
    }
  if((digitalRead(right_bt)==HIGH)&&(pressedr==0)&&(pressedl==0))
    {
      pressedr=1;
      motor();
      Mouse.press(MOUSE_RIGHT);
      delay(buttondelay);
    }  

   if((digitalRead(right_bt)==LOW)&&(pressedr==1))
   {
     pressedr=0;
     Mouse.release(MOUSE_RIGHT);
   }
    
}

