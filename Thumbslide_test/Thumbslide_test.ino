/*  Test program for configuring TSlide Mouse
    Copyright (C) 2018  ICFOSS

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/****************************************************************************
 * Program 1
 * *****************
 * This program is to be uploaded on to the TSlide Mouse to get configuration 
 * data required for the main firmaware: Mouse.ino 
 * Values to be noted down:
 * ->bottom_val_x ->top_val_x
 * ->bottom_val_y ->top_val_y
 * ->y_center     ->x_center
*/

long unsigned int timer1,timer2;
int x_small=0,x_big=0,y_small=0,y_big=0,x_center=0,y_center=0;
int x,y;
void setup()
{
  Serial.begin(9600);
  Serial.print("Initialsed");
}

void loop()
{
 while(!Serial.available())
 {
  Serial.println("Press any button and hit enter!");
  delay(300); 
 }
 Serial.println("Please leave the thumbslide joystick at the cetner position");
  x=analogRead(A2);
  y=analogRead(A3);
  Serial.print("Center at x=");Serial.print(x);Serial.print(" y=");Serial.println(y);
  Serial.println("Now Move the thumbslide joystick in all the directions for the next 8 seconds");
  timer1 = millis();
  timer2 = millis();
  while((timer2-timer1)<=8000)
  {
    timer2= millis();
    x=analogRead(A2);
    y=analogRead(A3);

    if(x<x_small)
      x_small = x;
    if(x<x_small)
      y_small = y;  

    if(x>x_big)
      x_big = x;
    if(y>y_big)
      y_big = y;    
  }
  Serial.println("Now leave the thumbslide joystick at the cetner position and be patient!");
  delay(5000);
  x=analogRead(A2);
  y=analogRead(A3);
  x_center = map(x,x_small+50,x_big-50,0,10);
  y_center = map(y,y_small+50,y_big-50,0,10);
  Serial.print("\n*#|");Serial.print(x_small+50);Serial.print("|");Serial.print(x_big-50);Serial.print("|");Serial.print(y_small+50);
  Serial.print("|");Serial.print(y_big-50);Serial.print("|");Serial.print(x_center);Serial.print("|");Serial.print(y_center);Serial.println("|#*");
  Serial.println("************************************************************************\nNote Down these values\n************************************************************************");
  Serial.print("Values Obtained:\n bottom_val_x:");Serial.print(x_small+50);Serial.print(" top_val_x:");Serial.println(x_big-50);
  Serial.print(" bottom_val_y:");Serial.print(y_small+50);Serial.print(" top_val_y:");Serial.println(y_big-50);
  Serial.print(" x_center =:");Serial.print(x_center);Serial.print(" y_center =:");Serial.println(y_center);
  while(1);
} 
