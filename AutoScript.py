import serial
import os
import sys
import glob
import time


def selectPort():
    if sys.platform.startswith('linux') or sys.platform.startswith('cygwin'):
        ports = glob.glob('/dev/tty[A-Za-z]*')
    else:
        raise EnvironmentError('Unsupported platform')

    result = []
    i = 0
    print("Ports with Arduino Pro Micro:\033[1;30;40m ")
    for port in ports:
        try:
            s = serial.Serial(port)
            s.close()
            if "ACM" in port:
                i = i+1
                print(i, ". ", port)
                result.append(port)
        except (OSError, serial.SerialException):
            pass
    if i == 0:
        print(colour("red"), "No device detected", colour("grey"), "\nListing all active ports:")
        for port in ports:
            try:
                s = serial.Serial(port)
                s.close()
                i = i+1
                print(i, ". ", port)
                result.append(port)
            except (OSError, serial.SerialException):
                pass

    select = int(input("\033[0;37;40m Please Select the port: "))
    select = select - 1
    port = result[select]
    try:
        s = serial.Serial(port)
        s.close()
        print("Port Selected!")
        return port
    except (OSError, serial.SerialException):
        print("Wrong Port Selected\nExiting program.")
        sys.exit


def execute(port):
    arduino = serial.Serial(port, 9600)
    while arduino.readline().strip().decode("ascii") != "Press any button and hit enter!":  # strip used to strip off newline and carriage return and decode is used to decode the bytes into ascii
        pass
    print("Arduino ProMicro Connected at port:", port)
    arduino.write(b'.')
    try:
        while True:
            ip = arduino.readline().strip().decode("ascii")
            if ip.startswith("*#|"):
                ls = ip.split("|")
                print(colour("yellow"), "Recieved Data:")
                print(colour("grey"), "top_val_x=", colour("yellow"), str(ls[2]), colour("grey"), "bottom_val_x=", colour("yellow"), str(ls[1]))
                print(colour("grey"), "top_val_y=", colour("yellow"), str(ls[4]), colour("grey"), "bottom_val_y=", colour("yellow"), str(ls[3]))
                print(colour("grey"), "x_center=", colour("yellow"), str(ls[5]), colour("grey"), "y_center=", colour("yellow"), str(ls[1]))

                return ls

            elif ip.startswith("Now Move the thumbslide joystick in"):
                print(colour("red"), " Now Move ", colour("grey"), "the thumbslide joystick", colour("yellow"), "in all the directions", colour("grey"), "for the next 8 seconds")
                print(colour("loading"))
                for i in range(8):
                    print(" ")
                    time.sleep(1)
                print(colour("grey"), "Completed", colour("white"))
            else:
                print(ip)
    except KeyboardInterrupt:
        pass


def colour(k):
    if k == "white":
        return "\033[0;37;40m"
    elif k == "grey":
        return "\033[1;30;40m"
    elif k == "yellow":
        return "\033[1;33;40m"
    elif k == "red":
        return "\033[1;31;40m"
    elif k == "loading":
        return "\033[0;31;42m"


if __name__ == "__main__":

    try:
        print(colour("grey"), "\nWelcome to", colour("yellow"), " TSlide Mouse", colour("grey"), " Configuration\n ", colour("white"), "Have you uploaded the Thumbslide_test.ino into the device ?")
        port = selectPort()
        ls = execute(port)
        print(colour("red"), "Creating Upload.ino", colour("grey"))
        outstring = ""
        inofile = open('Mouse/Mouse.ino', 'r')
        text = inofile.readlines()
        inofile.close()
        for lines in text:
            if lines.startswith('int top_val_x'):
                lines = "int top_val_x="+str(ls[2])+", bottom_val_x="+str(ls[1])+",top_val_y="+str(ls[4])+", bottom_val_y="+str(ls[3])+";"

            elif lines.startswith("x_center="):
                lines = "x_center="+str(ls[5])+",y_center="+str(ls[6])+";"

            outstring = outstring+lines
        if not os.path.exists('Upload/'):
            os.makedirs('Upload/')
        else:
            os.remove('Upload/Upload.ino')
        outfile = open('Upload/Upload.ino', 'w+')
        outfile.write(outstring)
        outfile.close()
        print("Upload the new Upload.ino using Arduino IDE")
    except KeyboardInterrupt:
        pass

print(colour("white"))
